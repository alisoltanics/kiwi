from django.contrib import admin

from .models import Post


class PostAdmin(admin.ModelAdmin):

    list_display = (
        'uuid', 'user', 'reply_to',
        'content', 'create_date', 'update_date'
    )

    readonly_fields = (
        'uuid', 'create_date', 'update_date'
    )


admin.site.register(Post, PostAdmin)
