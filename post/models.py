from django.db import models

from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

from utils.base_model import BaseModel


class Post(BaseModel):
    """
    Data model for Post.
    """

    user = models.ForeignKey(
        User, verbose_name=_("User"),
        on_delete=models.CASCADE
    )

    reply_to = models.ForeignKey(
        "post.Post", verbose_name=_("Post"),
        on_delete=models.CASCADE
    )

    content = models.TextField(
        verbose_name=_("Content"),
        max_length=360
    )

    class Meta:
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")

    def __str__(self):
        """
        returns a Unicode “representation” of
        Post object.
        """
        return self.user.username
