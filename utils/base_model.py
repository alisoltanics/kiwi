from django.db import models
from django.utils.translation import ugettext_lazy as _

from utils.helpers import create_short_uuid


class BaseModel(models.Model):

    class Meta:
        abstract = True

    uuid = models.CharField(
        verbose_name=_("UUID"), max_length=50,
        default=create_short_uuid, unique=True
    )

    update_date = models.DateTimeField(
        verbose_name=_("Update Date"),
        null=True, auto_now=True
    )

    create_date = models.DateTimeField(
        verbose_name=_("Create Date"),
        null=True, auto_now_add=True
    )
