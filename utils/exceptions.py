from rest_framework.exceptions import APIException


class UsernameUnavailableError(APIException):
    status_code = 406
    default_detail = 'This username is unavailable.'
    default_code = 'username_unavailable'


class NotSamePasswordError(APIException):
    status_code = 406
    default_detail = 'Entered passwords must be same.'
    default_code = 'password_issue'
