import shortuuid

from django.conf import settings


def create_short_uuid():
    shortuuid.set_alphabet(settings.SUUID_ALPHABET)
    suuid = shortuuid.ShortUUID().random(length=settings.SUUID_LENGTH)
    return suuid
