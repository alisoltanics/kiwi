from django.apps import AppConfig


class KiwiUserConfig(AppConfig):
    name = 'kiwi_user'
