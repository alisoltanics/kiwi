from django.contrib.auth.password_validation import validate_password
from django.core import exceptions

from rest_framework import serializers


class UserSignUpInputSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password1 = serializers.CharField(required=True)
    password2 = serializers.CharField(required=True)

    def validate(self, data):
        password = data.get('password1')
        errors = dict()
        try:
            validate_password(password=password)
        except exceptions.ValidationError as e:
            errors['password1'] = list(e.messages)
        if errors:
            raise serializers.ValidationError(errors)
        return super(UserSignUpInputSerializer, self).validate(data)
