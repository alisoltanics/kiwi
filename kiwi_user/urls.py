from django.urls import path
from .views import UserSignUpAPIView

urlpatterns = [
    path(
        'sign-up',
        UserSignUpAPIView.as_view(),
        name='user-sign-up'
    ),
]
