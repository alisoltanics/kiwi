from django.contrib.auth.models import User

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from .serializers import UserSignUpInputSerializer
from utils.exceptions import UsernameUnavailableError, NotSamePasswordError


class UserSignUpAPIView(APIView):
    """
    Creates new user.
    """

    def post(self, request: object):
        """
        Returns:
            response:
                HTTP_201_CREATED status code
        Raises:
            406 Not Acceptable:
                If user already exists and if
                two entered passwords are not same.
        """
        serializer = UserSignUpInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if User.objects.filter(
            username=serializer.data.get("username")
        ).first():
            raise UsernameUnavailableError

        if serializer.data.get(
            "password1"
        ) != serializer.data.get(
            "password2"
        ):
            raise NotSamePasswordError

        new_user = User.objects.create_user(
            username=serializer.data.get("username"),
            password=serializer.data.get("password1")
        )
        new_user.is_active = True
        new_user.save()
        return Response(status=status.HTTP_201_CREATED)
